package praktikum12;

import java.applet.Applet;
import java.awt.Color;
import java.awt.Graphics;

@SuppressWarnings("serial")
public class Yleminek extends Applet {
	
	@Override
	public void paint(Graphics g) {
		
		int w = getWidth();
		int h = getHeight();
		
		for (int y = 0; y < h ; y++) {
			double concentrtate = (double) y/h; // 0..1 liigub nullist �heni, mist�ttu algab mustast
			// a = 255...0
			int juice = (int) (concentrtate * 255);
			Color color = new Color(juice, juice, juice);  // new Color(a, a, a);
			g.setColor(color);
			
			g.drawLine(0, y, w, y);
			
		}


	}

}
