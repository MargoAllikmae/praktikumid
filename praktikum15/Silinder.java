package praktikum15;

public class Silinder extends Ring{

	double k6rgus;	
	
	public Silinder(Ring ring, double k6rgus) {
		super(ring.keskpunkt, ring.raadius);
	}
	
	@Override
	public double pindala() {
		 return super.pindala()*2 + ymberm66t() * k6rgus;
	}
	
	public double ruumala(){
		return super.pindala() * k6rgus;
		
	}

}
