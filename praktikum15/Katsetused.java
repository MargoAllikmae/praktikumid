package praktikum15;

public class Katsetused {
	
	public static void main(String[] args) {
		
		@SuppressWarnings("unused")
		String tekst = new String("Tere");
		
		Punkt p1 = new Punkt(2, 3);
		Punkt p2 = new Punkt(7, 9);
		
		System.out.println(p1);
		
		Joon j = new Joon(p1, p2);
		
		System.out.println(j);
		
		System.out.println("joone pikkus on: " + j.pikkus());
		
		Ring r = new Ring(p1, 12.3);
		System.out.println(r);
		
		Silinder s = new Silinder(r, 45.56);
		System.out.println("Silindir pindala on: " + s.pindala());
	}

}
