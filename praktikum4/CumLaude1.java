package praktikum4;

import lib.TextIO;

public class CumLaude1 {

	public static void main(String[] args) {

		System.out.println("Palun sisesta l�put�� hinne");
		double loputoo = TextIO.getlnDouble();
		
		if(loputoo < 0 || loputoo >5){
			System.out.println("Vigane sisetus!");
			return; //v�ljuda meetodist ja katkestada programm
		}
		
		System.out.println("Palun sisesta keskmine hinne");
		double keskmine = TextIO.getlnDouble();
		
		if(keskmine < 0 || keskmine >5){
			System.out.println("Vigane sisetus!");
			return; //v�ljuda meetodist
		}
	

		if (keskmine > 4.5 && loputoo == 5) {
			System.out.println("Saad Cum Laude!");
		} else {
			System.out.println("Ei saa Cum Laudet!");
		}

	}

}
