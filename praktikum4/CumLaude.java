package praktikum4;

import lib.TextIO;

public class CumLaude {

	public static void main(String[] args) {

//		System.out.println("Palun sisesta l�put�� hinne");
//		int loputoo = TextIO.getlnInt();
		System.out.println("Palun sisesta keskmine hinne");
		double keskmine = TextIO.getlnDouble();
		
		// boolean korral alati == v�rdusm�rki
		
		//! - eitus
		//== - v�rdlus
		//&& - loogiline JA
		//|| - loogilne V�I
		//>= - suurem v�rdne
		
		if (keskmine > 5 || keskmine < 0 ){
			System.out.println("Vigane hinne");
		}
		
		if(keskmine == 5) {				
			System.out.println("WOW!");
		} else if (keskmine > 4.7) {
			System.out.println("Super hea");
		} else if (keskmine > 4.5) {
			System.out.println("V�ga hea keskmine hinne");
		} else {
			System.out.println("Kesine keskmine hinne");
		}

	}

}
