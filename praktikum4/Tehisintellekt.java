package praktikum4;

import lib.TextIO;

public class Tehisintellekt {
	
	public static void main(String[] args) {

		System.out.println("Siesta esimese kasutaja vanus ");
		double user1 = TextIO.getlnDouble();

		System.out.println("Sisesta teise kasutaja vanus ");
		double user2 = TextIO.getlnDouble();

		double userage = user1 - user2;

		if (userage > 10 || userage < -10) {
			System.out.println("Sobimtault suur vahe");
		} else if (5 <= userage || userage <= -5) {
			System.out.println("Suur vahe");
		} else {
			System.out.println("Vahe on OK");
		}

	}

}
