package praktikum4;

import lib.TextIO;

public class ParooliKontroll {
	
	public static void main(String[] args) {
		
		System.out.println("Paluks sisestada parool");
		String parool1 = TextIO.getlnString();
		
		String parool2 = "Margo";
		
		if (parool1.equals(parool2)) {			//sisestab tagasi boolean meetodi
			System.out.println("Parool on �ige!");
		} else{
			System.out.println("Parool on vale!");
		}
		
	}

}
