package praktikum4;

import lib.TextIO;

public class Tehisintellekt1 {
	
	public static void main(String[] args) {
		
		System.out.println("Palun sisesta kaks vanust");
		int age1 = TextIO.getlnInt();
//		if (age1<0) {
//			System.out.println("Vanus ei saa olla negatiivne");
//			return;
//		}
		int age2 = TextIO.getlnInt();
//		if (age2<0) {
//			System.out.println("Vanus ei saa olla negatiivne");
//			return;
//		}

		/*
		 * saab lahendad ka nii, et negatiivse vastusekorral annab veateate
		 */
		
		int vanuseVahe = Math.abs(age1 - age2);

		/*
		 * Math.abs - annab alati tulemuse postiivse isegi negatiivse tulemusel
		 */

		// System.out.println(vanuseVahe);

		if (vanuseVahe > 10) {
			System.out.println("Midagi v�ga kr�bedat");
		} else if (vanuseVahe > 5) {
			System.out.println("No ikka ei sobi");
		} else {
			System.out.println("Sobib!");
		}

	}

}
