package praktikum4;

import lib.TextIO;

public class ParooliKontroll1 {
	
	public static void main(String[] args) {
		
		String oigeParool = "margo";
		int guess =0;
		
		/*
		 * kui quess asemel on true and quess++ ei ole l�ppus, siis k�sib parooli mitmeid kordij�rest  
		 */
		
		while (guess <3) {  

			System.out.println("Sisesta parool: ");
			String sisestatudParool = TextIO.getlnString();

			if (oigeParool.equals(sisestatudParool)) {
				System.out.println("�ige parool!");
				break;  
			} else {
				System.out.println("Vale parool!");
			}

			guess++;
		}
		
		System.out.println("kolme katsega �iget koodi tabada ei �nnestunud!");
	}
	
	
}
