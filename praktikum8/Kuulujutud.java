package praktikum8;

import praktikum6.Meetodid;

public class Kuulujutud {
	
	public static void main(String[] args) {
		
		String[] meheNimed = {"Juku", "Kalle", "Peeter"}; 
		String[] naiseNimed = {"Jaana", "Kati", "Karin"}; 
		String[] tegus6na = {"laulavad", "jooksevad", "jalutavd"}; 
		
		
		String mees = suvalineElement(meheNimed);
		String naine = suvalineElement(naiseNimed);
		String tegevus = suvalineElement(tegus6na);
		
		
			System.out.format("%s ja %s %s.", mees, naine, tegevus);
		
		}

	private static String suvalineElement(String[] s6nad) {
		return s6nad[Meetodid.suvalineArv(0, s6nad.length-1)];
	}	
	

}
