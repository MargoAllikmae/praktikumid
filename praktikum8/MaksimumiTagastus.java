package praktikum8;

public class MaksimumiTagastus {
	
	public static void main(String[] args) {
		
		int [] arvud = {22, 33, 44, 77};
		
		for (int i = 0; i < arvud.length; i++) {
			
			System.out.println(arvud[i]);
			
		}
		
		int arv = maksimum(arvud);
		
		System.out.format("Maksimum %s." ,arv);
		
	}

	 
	private static int maksimum(int[] arvud) {
		
		int maxArv = arvud[0];
		
		for (int i = 1; i < arvud.length; i++) {
			if(arvud[i] > maxArv){
				maxArv = arvud[i];
			}
			
		}
		
		return maxArv;
	}

}
