package praktikum8;

public class MaksimumiTagastus1 {
	
	public static void main(String[] args) {
		
		int [][] arvud = {{22, 99, 100, 77,  177},{105, 88, 175, 120}};
		
		for (int i = 0; i < arvud.length; i++) {
		    for (int j = 0; j < arvud[i].length; j++) {
		        System.out.println(arvud[i][j] + " ");
		    }
		    System.out.println();
		}
		
		int arv = maksimumMatriks(arvud);
		
		System.out.format("Maksimum %s." ,arv);
		
	}

	 
	private static int maksimumMatriks(int[][] arvud) {
		
		int maxArv = 0;
		
		for (int i = 0; i < arvud.length; i++) {
			for (int j = 0; j < arvud[i].length; j++) {
				if(arvud[i][j] > maxArv){
					maxArv = arvud[i][j];
			}
			
			}
			
		}
		
	return maxArv;
	
	}

}
