package praktikum1;

public class SuurT2htLugemine {

	public static int loenda (String s) {
		int res =0;
		for (int i=0; i<s.length();i++) {
			if(Character.isUpperCase(s.charAt(i)))
			res++;
		}
		return res;
	}
	
	public static void main(String[] args) {
		System.out.println(loenda("Tere Hommikut, MARGO"));
	}
	
}
