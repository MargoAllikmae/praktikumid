package praktikum6;

public class Meetodid1 {
	
	public static void main(String[] args) {
		
		// 0 -- kull, 1 -- kiri
		int kasutajaArvab = Meetodid.kasutajaSisestus("Sisesta kull (0) v�i kiri (1)", 0, 1);
		
		int myndiVise = (Math.random() > 0.5) ? 0 : 1; // l�hike if lause
		
		System.out.println("Kasutaja arvab: " + kasutajaArvab); //
		System.out.println("Arvuti arvab: " + myndiVise); // kontrolliks
		
		if (myndiVise == kasutajaArvab) {
			System.out.println("Arvasid �ra!");
		} else {
			System.out.println("M��da panid!");
		}
		
	}

}
