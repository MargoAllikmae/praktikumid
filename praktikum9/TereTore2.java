package praktikum9;

import lib.TextIO;

public class TereTore2 {
	
	public static void main(String[] args) {
		
		System.out.println("Palun siesta sona");
		String sona = TextIO.getlnString();
		
		for (int i = 0; i < sona.length(); i++) {
			if(i>0){
				System.out.print("-");
			}
			System.out.print(sona.toUpperCase().charAt(i));
		}
	}

}
