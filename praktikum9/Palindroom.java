package praktikum9;

import lib.TextIO;

public class Palindroom {

	public static void main(String[] args) {
		
		System.out.println("Siesta s�na");
		String s6na =TextIO.getlnString();
		
		if(onPalindroom(s6na)){
			System.out.println("See s�na on palidroom");
		} else {
			System.out.println("Ei ole palidroom");
		}
		
	}

	private static boolean onPalindroom(String s6na) {
		return s6na.equals(tagurpidi(s6na));
	}
	
	public static String tagurpidi(String oigetpidi){
		String tagurpidi = "";
		for (int i = 0; i < oigetpidi.length(); i++) {
			tagurpidi = oigetpidi.charAt(i)+ tagurpidi;
		}
		return tagurpidi;
	}
	
}
