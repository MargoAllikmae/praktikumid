package praktikum5;

import lib.TextIO;

public class Tsyklid5 {

	public static void main(String[] args) {

		System.out.println("Palun sisesta tabeli suurus");
		int tabeliSuurus = TextIO.getInt();

		tryki22Ris(tabeliSuurus);

		for (int i = 0; i < tabeliSuurus; i++) {

			System.out.print("| ");

			for (int j = 0; j < tabeliSuurus; j++) {
				if (i == j || j + i == tabeliSuurus - 1) {
					System.out.print("x ");
				} else {
					System.out.print("0 "); // print k�sk prindib �hele reale
											// println - tr�kkib teksti read
											// teineteise j�rgi
				}

				System.out.print("(i:" + i + " j:" + j + ")"); // n�en �ra
				// kuidas programm numberid annab
			}
			System.out.print("| ");
			System.out.println(); // reavahetus
		}

		tryki22Ris(tabeliSuurus);

	}

	private static void tryki22Ris(int tabeliSuurus) {
		for (int i = 0; i < tabeliSuurus * 2 + 3; i++) {
			System.out.print("-");
		}
		System.out.println();

	}

}
