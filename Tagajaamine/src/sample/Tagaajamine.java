package sample;

/**
 * Created by HP on 28.01.2017.
 */
public class Tagaajamine extends Application{

    @Override
    public void start(Stage primaryStage) throws Exception {
        Pane pane = new Pane();
        @SuppressWarnings("unused")
        Scene scene = new Scene(pane, 500, 500);

        for (int i = 0; i < 10; i++) {
            Rectangle rect = new Rectangle(i*100, i*100, 100, 100);
            pane.getChildren().add(rect);
            rect.setOnMouseClicked(event ->{
                if (rect.getId() == ("klikitud")) {
                    rect.setScaleX(0);
                    rect.setScaleY(0);
                } else {
                    rect.setScaleX(0.5);
                    rect.setScaleY(0.5);

                }

                rect.setId("klikitud");
            });

        }

    }



}