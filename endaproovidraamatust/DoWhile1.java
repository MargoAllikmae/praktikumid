package endaproovidraamatust;

import lib.TextIO;

public class DoWhile1 {
	
	public static void main(String[] args) {
		
	
		boolean wantsToContinue;  // True if user wants to play again.
		do {
//			 Checkers.playGame(); 
		   System.out.print("Do you want to play again? ");
		   wantsToContinue = TextIO.getlnBoolean();
		} while (wantsToContinue == true);
		
	}

}
