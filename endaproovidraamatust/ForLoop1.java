package endaproovidraamatust;

public class ForLoop1 {
	
	public static void main(String[] args) {
		
		int i;
		int j;
		
		for ( i=1, j=10;  i <= 10;  i++, j-- ) {
			   System.out.printf("%5d", i); // Output i in a 5-character wide column.
			   System.out.printf("%5d", j); // Output j in a 5-character column 
			   System.out.println();       //     and end the line.
			}
		
	}

}
