package endaproovidraamatust;

public class WhileLoop {
	
	public static void main(String[] args) {
		
		int number;
		number=2;
		
		while (number < 10 ) {					// loop k�ib n�ndakaua kui while sulgudes osutub valeks, siis l�petab
												// 
			System.out.println(number);
			number =number +1;
					
		}
		
		System.out.println("Done!");			// kui saab number on 10 siis while sulgudes olev statment on vale ja program l�petab t��
		System.out.println(number);
		
	}

}
