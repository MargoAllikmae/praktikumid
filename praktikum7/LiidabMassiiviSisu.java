package praktikum7;

public class LiidabMassiiviSisu {
	
	public static void main(String[] args) {
		
		int [] arvud = {5, 3, 5, 5, 4, 5};
		
		System.out.println(massiiviliitmine(arvud));
	}

	private static int massiiviliitmine(int[] arvud) {
		int arv = 0;
		for (int i = 0; i < arvud.length; i++) {
			arv += arvud[i]; 
		}
		return arv;
	}

}
