package praktikum13;


public class Massiivid {
	
	public static void main(String[] args) {
		
//		int[] arvud = {3, 4, 6, -3, 9}; // ühene massiiv
		
		int[][] neo = {
				{1, 1, 1, 1, 1},
				{2, 3, 4, 5, 6},
				{3, 4, 5, 6, 7},
				{4, 5, 6, 7, 8},
				{5, 6, 7, 8, 9},
		};
		
//		System.out.println(arvud[1]);
//		tryki1(arvud);
//		tryki4(neo);
//		tryki(ridadeSummad(neo));
//		pealDiagonaaliSumma(neo);
		System.out.println(peaDiagonaaliSumma(neo));
		tryki(ridadeMaksimumid(neo));
		
//		tryki3(arvud);
		
	}
	
	/*
	 * Kirjutage meetod, mis trükib ekraanile ühele real
	 * parameetrina etteantud ühemõõtmelise täisarvumassiivi elemendid
	 */
	public static void tryki(int[] massiiv) {
		for (int i = 0; i < massiiv.length; i++) {
		System.out.println(massiiv[i] + " ");
		 }
		
	}
	
		public static void tryki1(int[] massiiv) {
			for (int arv : massiiv) {
			System.out.print(arv + " ");
			 }
		
			System.out.println();
	}
		
	/*
	 * Kirjutage meetod, mis trükib ekraanile parameetrina 
	 * etteantud kahemõõtmelise massiivi(maatriksi)
	 * 
	 * Kasuta maatrikis rea trükkimseks kindlasti eelnevalt kirjutatud meetodit! 
	 */
	
	public static void tryki4(int[][] maatriks) {
		for(int[]rida:maatriks){
			tryki1(rida);
		}
//		System.out.println("kahemõõteline maatriksi trükkimine");
		
	}
	
	/*
	 * Arvutada maatriksi iga rea elementide summa
	 */
	
	public static int[] ridadeSummad(int[][] maatriks) {
		int[] summad = new int[maatriks.length];
		for (int i = 0; i < maatriks.length; i++) {
			summad[i] = reaSumma(maatriks[i]);
		}
		
		return summad;
	}
	
	//TODO eraldi meetodi rea summa arvutamiseks
	public static int reaSumma(int[] massiiv){
		int summa =0;
		for (int arv :massiiv) {
			summa += arv;
		}
		return summa;
	}
	
	/*
	 * arvutada kõrvaldiagonaalide elementide summa 
	 * (kõrvaldiogonaal on see, mis jookseb ülevalt paremast nurgast alla vasakusse nurka)
	 */
	public static int korvalDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		return summa;
		
	}
	
	public static int peaDiagonaaliSumma(int[][] maatriks) {
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			for (int j = 0; j < maatriks[i].length; j++) {
				if (i==j){
					summa += maatriks[i][j];
				}
				System.out.print(maatriks[i][j]+ "(i="+ i + "j="+j+ ") ");
			}
			System.out.println();
		}
		return summa;
		
	}
	
	public static int peaDiagonaaliSummaEfektiivsemalt(int[][] maatriks){
		int summa = 0;
		for (int i = 0; i < maatriks.length; i++) {
			summa += maatriks[i][i];
		}
		return summa;
	}
	
	/*
	 * Leida iga rea suurim element
	 */
	
	public static int [] ridadeMaksimumid(int[][] maatriks) {
		int [] maksimumid = new int[maatriks.length];
		for (int i = 0; i < maksimumid.length; i++) {
			maksimumid [i] = reaMaksimum(maatriks[i]);
		}
		return maksimumid;
	}
	
	public static int reaMaksimum(int[] massiiv) {
		int maksimum = Integer.MIN_VALUE;
		for (int arv : massiiv){
			if(arv>maksimum){
				maksimum = arv;
			}
		}
		return maksimum;
	}
		
}
