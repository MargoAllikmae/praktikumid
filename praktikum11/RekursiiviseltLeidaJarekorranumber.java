
package praktikum11;

public class RekursiiviseltLeidaJarekorranumber {
	
	public static void main(String[] args) {
		System.out.println(fibb(6));
		
	}

	public static int fibb(int a) {
		if (a <=1) {
			return a;
		}
		
		return fibb(a-1) + fibb(a-2);
	}
	
}

//	k�ivtan meetodi			tulemus
//	    (6)					(?+?)

//		(5)					(?+?)
//		(4)					(?+?)

//		(4)					(?+?)
//		(3)					(?+?)
//		(3)					(?+?)
//		(2)					(?+?)

//		(3)					(?+?)
//		(2)					(?+?)
//		(2)					(?+?)
//		( )					(?+?)
//		( )					(?+?)
//		( )					(?+?)
//		( )					(?+?)
//		( )					(?+?)