package eksamiUleasndedMaatriks;

import java.util.Arrays;

/**
 * Loo 10*10 maatriks tagurpidi Tsirkuse lauam�nguga. Ehk numbrid �hest sajani
 * alustavad �levalt paremalt ja j�uavad alla paremale (v�i kui kaugele iganes).
 *
 * N�ide 5*5 laual:
 *  5  4  3  2  1
 *  6  7  8  9 10
 * 15 14 13 12 11
 * 16 17 18 19 20
 * 25 24 23 22 21
 * 26 27 28 29 30
 */
public class Sukrist {

    // Lihtsalt abiline meetod, et maatriksit v�lja printida
    private static void printMaatriks(int[][] laud) {
        for (int i = 0; i < laud.length; i++) {
            System.out.println(Arrays.toString(laud[i]));
        }
        System.out.println("");
    }
}