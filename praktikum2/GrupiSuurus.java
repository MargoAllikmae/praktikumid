package praktikum2;

import lib.TextIO;

//Ctrl + Shift + F  - paneb programmi korralikult "treppi"

public class GrupiSuurus {

	public static void main(String[] args) {

		System.out.println("Sisesta inimeste arv");
		int inimesteArv = TextIO.getlnInt();

		System.out.println("Sisesta gruppi suurus");
		int grupiSuurus = TextIO.getlnInt();

		int gruppideArv = inimesteArv / grupiSuurus;
		System.out.println("Saab moodustada " + gruppideArv + " gruppi");

		int j22k = inimesteArv % grupiSuurus;
		System.out.println("�le j��b " + j22k + " inimest");
	}

}
