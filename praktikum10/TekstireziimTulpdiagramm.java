package praktikum10;

import java.util.ArrayList;
import java.util.Scanner;

public class TekstireziimTulpdiagramm {
	
	private static Scanner sc;

	public static void main(String[] args) {

		sc = new Scanner (System.in);
		ArrayList<Integer> numbrid = new ArrayList<Integer>();
		int  suurim = Integer.MIN_VALUE;
		while (true) {
			System.out.println("�tle number");
			int input = sc.nextInt();
			if (input==0) {
				break;
			}
			if(input > suurim)
				suurim = input;
			numbrid.add(input);
		}
		
		double kordaja = 10.0 / suurim;
		for (int i = 0; i < numbrid.size(); i++) {
			int nr = numbrid.get(i);
			System.out.printf("%2d %s\n", nr, genereeriIksid(nr, kordaja));
		}
	}

	public static String genereeriIksid(int nr,double kordaja) {
		String iksid = "";
		for (int i = 0; i < nr * kordaja; i++) {
			iksid = iksid + "x";
		}
		
		return iksid;
	}
}
