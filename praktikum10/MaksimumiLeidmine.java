package praktikum10;

public class MaksimumiLeidmine {
	public static void main(String[] args) {
		int[] massiiv = { 1, 3, 6, 7, 8, 3, 5, 7, 27, 3 };
		int suurim = Integer.MIN_VALUE;  // annab v�iksema v��rtuse v�lja
		for (int i = 0; i < massiiv.length; i++) {
			if (suurim < massiiv[i]) {
				suurim = massiiv[i];
			}
		}
		System.out.println(suurim);
	}

}
