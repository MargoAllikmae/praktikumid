package praktikum10;

import java.util.ArrayList;

public class MaksimumiLeidmine1 {
	public static void main(String[] args) {
		int[][] neo = {
				{ 1, 3, 6, 7},
				{ 2, 3, 3, 1},
				{ 17, 4, 5, 0},
				{ -20, 13, 16, 17},
		};
		
		ArrayList<Integer> suurimad = new ArrayList<Integer>();
		for (int i = 0; i < neo.length; i++) {
			suurimad.add(suurimMassiivist(neo[i]));
		}
		
		// ArrayList -> Massiiviks
		int[] suurimadMassiiv = new int[suurimad.size()]; 
		for (int i = 0; i < suurimadMassiiv.length; i++) {
			suurimadMassiiv[i] = suurimad.get(i); 
		}
		
		//L�ppvastus
		int suurim = suurimMassiivist(suurimadMassiiv);
		
		System.out.println(suurim);
		
	}

	private static int suurimMassiivist(int[] massiiv) {
		int suurim = Integer.MIN_VALUE; 
		for (int i = 0; i < massiiv.length; i++) {
			if (suurim < massiiv[i]) {
				suurim = massiiv[i];
			}
		}
		return suurim;
	}

}
