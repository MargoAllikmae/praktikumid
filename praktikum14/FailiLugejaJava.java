package praktikum14;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;


public class FailiLugejaJava {
	public static void main (String [] args) {

	    // punkt t�histab jooksvat kataloogi
	    String kataloogitee = FailiLugejaJava.class.getResource(".").getPath();
		System.out.println(kataloogitee);
	    
	    // otsime samast kataloogist kala.txt-nimelist faili
		File file = new File(kataloogitee + "kala.txt");
		
		try {
		    // avame faili lugemise jaoks
			@SuppressWarnings("resource")
			BufferedReader in = new BufferedReader(new FileReader(file));
			String rida;

			// loeme failist rida haaval
			while ((rida = in.readLine()) != null) {
				System.out.println(rida);
			}
		}
		catch (FileNotFoundException e) {
		    System.out.println("Faili ei leitud: \n" + e.getMessage());
		}
		catch (Exception e) {
			System.out.println("Error, jee, mingi muu error: " + e.getMessage());
		}
	}
	
	
	
	

}
