package loenguNaitedJava;

//-----------------------------------------------------
//Kellad.java
//-----------------------------------------------------

/**
 * Peameetodit sisaldav silumisklass. Siis saab k�ik alguse. 
 * @author M
 * @version 1.0
 *
 */

public class Kellad {
	
		/**
		 * Peameetod. K�ivitab teisi meetodeid.
		 * @param s k�surealt etteantud tekst
		 */
	

	   public static void main (String[] s) {
		      Kell k = new KaeKell ("Rolex");
		      System.out.println (((KaeKell)k).mark 
		         + " " + k.jooksevAeg());
		      Mobla m = new Mobla (25612345);
		      System.out.println (String.valueOf (m.number)
		         + " " + m.jooksevAeg());
		   } //main
		} // Kellad

/**
 * Liides, mis kirjeldab oskust aega n�idata. Pole oluline, mis on ajalugemise allikas
 * @version 0.7alpha
 */
		interface Ajanaitaja {
			
			/**
			 * Hetekaeg teksitna. Paljud seadmed oskavad aega n�idata.
			 * @return jooksev aeg
			 */
		   String jooksevAeg();
		}

//		class Kell implements Ajanaitaja {
//		   public String jooksevAeg() {
//		      return new java.util.Date().toString();
//		   }
//		}

		@SuppressWarnings("serial")
		/**
		 * K�ekekellade omadusi kijreldav klass. K�ekellal on tootja ouline
		 * @author HP
		 *
		 */
		class KaeKell extends Kell {
			/**k�ekella mark */
		   String mark;
		   /**k�ekella konstruktor
		    * @param s tootjafirma nimi
		    */
		   KaeKell (String s) {
		      mark = s;
		   } // konstruktor
		   
		} // KaeKell

		/**
		 * 
		 */
		class Telefon {
		   int number;
		   Telefon (int n) {
		      number = n;
		   }
		}

		class Mobla extends Telefon implements Ajanaitaja {
		   Kell sisemineKell;
		   Mobla (int n) {
		      super (n);
		      sisemineKell = new Kell();
		   }
		   public String jooksevAeg() {
		      return sisemineKell.jooksevAeg();
		   }
	
}
