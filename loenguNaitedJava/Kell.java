package loenguNaitedJava;

import java.applet.Applet;   // meie kell on vormilt rakend
import java.awt.*;
import java.awt.event.*;
import java.util.Date;       // meie kell kasutab Date va"ljundit

/** lo'imena realiseeritud kell. */

public class Kell extends Applet implements Runnable {


	   /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	/** kell sisaldab lo'ime. */
	   private Thread loim;

	   /** kella yhe tiksu pikkus millisekundites. */
	   int tiks = 1000;

	   /** peameetod. */
	   public static void main (String[] parameetrid) {
	   // saame ka ka"surealt ka"ivitada
	      Frame raam = new Frame ("Minu tehtud kell");
	      raam.setSize (350,80);
	      Kell minuKell = new Kell();
	      raam.add (minuKell);
	      raam.setVisible (true); // kell ekraanile
	      raam.addWindowListener (new WindowAdapter () {
	         public void windowClosing (WindowEvent e) {
	            System.exit (0);
	         }
	      } );
	      minuKell.run(); // kell ka"ima!
	   }

	   /** realiseerime liidese Runnable. */
	   public void run() {
	      while (true) {
	         try {
	            Thread.sleep (tiks); //magamisaeg on millisekundites
	         }
	         catch (InterruptedException e) {
	         }
	         repaint(); // kella seisu va"rskendamine
	      }
	   }

	   /** ylekatmine, et teksti ekraanile saada. */
	   public void paint (Graphics ekraan) { // Container meetod
	      ekraan.drawString (new Date().toString(), 50, 25);
	      // jooksev aeg stringina va"ljastatakse antud kohta
	   }

	   /** see on rakendi meetod. */
	   @SuppressWarnings("deprecation")
	public void start() {
	      if (loim == null) {
	         loim = new Thread (this); // "this" on meil Runnable
	         loim.start();             // seega this.run() la"heb ka"ima
	      } else {
	         loim.resume(); //loim tegevus taastatakse
	      }
	   }

	   /** see on rakendi meetod. */
	   @SuppressWarnings("deprecation")
	public void stop() {
	      loim.suspend();
	   }

	   /** see on rakendi meetod. */
	   @SuppressWarnings("deprecation")
	public void destroy() {
	      if (loim != null) {
	         loim.stop();      // Thread-klassi stop()
	         loim = null;      // kuulutame surnuks
	      } //else ongi surnud
	   }

	public String jooksevAeg() {
			      return new java.util.Date().toString();
			   
	}

	} // Kell lopp

