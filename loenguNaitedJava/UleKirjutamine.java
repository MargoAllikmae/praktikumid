package loenguNaitedJava;

public class UleKirjutamine {

	/**
	 * Na"ide pa"rimisest ja meetodite u"lekatmisest.
	 * @author Jaanus Poial
	 * @since 1.5
	 * @version 0.4
	 */

	   /** P6hiprogramm. */
	   static public void main (String[] argumendid) {
	      System.out.println ("Mida loomad ytlevad?");
	      Loom minuLoom = new Loom ("Patu");
	      System.out.println (minuLoom); //kasutab toString() meetodit
	      Kass minuKass = new Kass ("Kiti");
	      System.out.println (minuKass);
	      minuLoom = minuKass;
	      System.out.println (minuLoom);
	      Koer minuKoer = new Koer ("Muri");
	      System.out.println (minuKoer);
	   } //main lopp

	} //Loomad lopp

	/** Ylemklass, tegelikult java.lang.Object alamklass. */
	class Loom {

	   /** k6igil loomadel on nimi */
	   private String nimi;

	   /** konstruktor */
	   Loom (String s) {
	      paneNimi (s);
	   }

	   /** getter */
	   public String votaNimi() {
	      return nimi;
	   }

	   /** setter */
	   public void paneNimi (String s) {
	      nimi = s;
	   }

	   /** teisendus s6neks */
	   @Override
	   public String toString() { //katame yle
	      return "Olen loom " + votaNimi();
	   }

	} // Loom lopp

	/** Klassi Loom alamklass. */
	class Kass extends Loom { 

	   /** Kass omab atribuuti vurrupikkus */
	   int vurrupikkus;

	   /** konstruktor */
	   Kass (String s) {
	      super (s);
	   }

	   /** ylekaetud teisendus s6neks */
	   @Override
	   public String toString() { //katame kaetu veel yle
	      return "Olen KASS " + votaNimi();
	   }

	} // Kass lopp

	/** Klassi Loom alamklass. */
	class Koer extends Loom { //teine alamklass

	   /** kas on saba */
	   boolean sabaga;

	   /** konstruktor */
	   Koer (String s) {
	      super (s);
	      sabaga = true;
	   }

	} // Koer lopp	
	

