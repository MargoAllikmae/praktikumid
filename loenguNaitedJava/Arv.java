package loenguNaitedJava;

import java.util.ArrayList;
import java.util.Collections;

public class Arv implements Comparable<Arv>{
	
	/** N&auml;iteobjekt, mis sisaldab <code> int </code> t&uuml;&uuml;pi 
	 * v&auml;lja. Isendid on v&otilde;rreldavad suuruse j&auml;rgi.
	 * @author Jaanus P&ouml;ial
	 * @since 1.5
	 */
	   /** Isendimuutuja. */
	   private int sisu = 0;

	   /** Konstruktor uue objekti loomiseks. */
	   Arv (int i) {
	      sisu = i;
	   }

	   /** Teine konstruktor, mis loob objekti stringist. */
	   Arv (String s) {
	      sisu = Arv.parseArv (s);
	   }

	   /** Avalik meetod andmete lugemiseks objektist. */
	   public int arvValue() {
	      return sisu;
	   }

	   /** Objekti esitus stringina. */
	   @Override
	   public String toString() {
	      return String.valueOf (this.arvValue());
	   }

	   /** Kas kaks objekti on sisu poolest v&otilde;rdsed. */
	   @Override
	   public boolean equals (Object o) {
	      int osisu = ((Arv)o).arvValue();
	      return this.arvValue() == osisu;
	   }

	   /** Liidese Comparable kohustuslik meetod. */
	   public int compareTo (Arv o) {
	      int osisu = ((Arv)o).arvValue();
	      if (this.arvValue() == osisu)
	         return 0;
	      else
	         if (this.arvValue() > osisu)
	            return 1;
	         else
	            return -1;
	   }

	   /** Koopia tegemine. */
	   @Override
	   public Object clone() {
	      return new Arv (arvValue());
	   }

	   /** Vabrik. */
	   public static Arv valueOf (int i) {
	      return new Arv (i);
	   }

	   /** Teine vabrik stringist loomiseks. */
	   public static Arv valueOf (String s) {
	      return new Arv (s);
	   }

	   /** Teisendus stringist arvuks. */
	   public static int parseArv (String s) {
	      return Integer.parseInt (s);
	   }

	   /** Kahe arvu liitmine. */
	   public Arv pluss (Arv a) {
	      return new Arv (this.arvValue() + a.arvValue());
	   }
	   // teised tehted tehakse analoogiliselt liitmisega

	   /** Peameetod. Rakenduse kohustuslik meetod. */
	   public static void main (String[] args) {
	      if (args.length > 0) {
	         int i = Arv.parseArv (args[0]);
	         System.out.println (args[0]);
	         System.out.println (Arv.valueOf (i).toString());
	         System.out.println (Arv.valueOf (args[0]).toString());
	      }
	      Arv a1 = new Arv (2);
	      Arv a2 = Arv.valueOf (-1);
	      Arv a3 = (Arv)a2.clone();
	      System.out.println ("Peab olema true:");
	      System.out.println (a2.equals (a3));
	      System.out.println (a2 != a3);
	      System.out.println (a2.compareTo (a3) == 0);
	      System.out.println (a2.equals (a2));
//	      System.out.println (a2 == a2);
	      System.out.println (a2.compareTo (a2) == 0);
	      System.out.println (a1.compareTo (a2) > 0);
	      System.out.println (a2.compareTo (a1) < 0);
	      System.out.println (!a1.equals (a2));
	      System.out.print (a1 + " + " + a2 + " = ");
	      System.out.println (a1.pluss (a2));
	      ArrayList<Arv> l = new ArrayList<Arv>();
	      for (int i = 0; i < 10; i++) {
	         l.add (new Arv ((int)(Math.random()*100.)));
	      }
	      System.out.println (l);
	      Collections.sort (l);
	      System.out.println (l);
	   }

	}


