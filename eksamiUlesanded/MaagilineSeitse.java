package eksamiUlesanded;

/**
 * Antud on int[] massiiv. Korruta k�ik seitsmed kahega ja leia arvude keskmine.
 */

public class MaagilineSeitse {

	public static void main(String[] args) {

		// int [] naide = {7, 4, 324, 65, 4, 78, 7, 45, 4};

		int n = 2;
		int[] intArray = new int[] { 7, 4, 324, 65, 4, 78, 7, 45, 4 };

		for (int i = 0; i < intArray.length; i++) {

			if (intArray[i] == 7) {
				intArray[i] *= n;
			}

			System.out.println(intArray[i]);
		}

		int summa = 0;
		for (int arv : intArray) {
			summa += arv;
		}

		System.out.println(summa);
		double tulemus = summa / intArray.length;
		System.out.println(tulemus);
		System.out.printf("%1.2f%n", tulemus);
	}

}


