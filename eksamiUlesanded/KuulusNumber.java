package eksamiUlesanded;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;


/**
 * Antud on int[] massiiv. Eemalda k�ik nr 3 v��rtused ja leia millist numbrit esineb k�ige siis rohkem?
 * Mitte siis number, mis on kolmandal indeksil, vaid nr kolm ise.
 */

public class KuulusNumber {
	
    public static void main(String[] args) {
    	
    	Integer [] naide = {1,5,3,3,6,3,7,7};
    	
    	List<Integer> list = new ArrayList<Integer>();
    	list.addAll(Arrays.asList(naide)); 
    	
    	 @SuppressWarnings("rawtypes")
		Iterator itr = list.iterator();
         while (itr.hasNext())
         {
             int x = (Integer)itr.next();
             if (x == 3)
                 itr.remove();
         }
         
         System.out.println(list);
         
         Map<Integer, Integer> list1 = new HashMap<Integer, Integer>();
         for (int i : list) {
             Integer count = list1.get(i);
             list1.put(i, count != null ? count+1 : 0);
         }

         System.out.println(list1);
         
         Integer popular = Collections.max(list1.entrySet(),
        		    new Comparator<Map.Entry<Integer, Integer>>() {
        						@Override
					public int compare(Entry<Integer, Integer> o1, Entry<Integer, Integer> o2) {
						 return o1.getValue().compareTo(o2.getValue());
					}
        		}).getKey();
         
         System.out.println(popular);
    }
 	
}
    
    
