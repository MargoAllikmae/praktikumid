package eksamiUlesanded;

/**
 * Antud on kaks massiivi �hu temperatuuridega. Arvuta m�lema puhul mitu
 * positiivset temperatuuri on massiivis rohkem kui negatiivseid.
 */

public class Suusailm {

	public static void main(String[] args) {

		int[] kraad1 = new int[] { 11, 10, 5, 1, 3, -2, -6, 2, -13, -24, -9, 0 };
		int[] kraad2 = new int[] { -23, -25, -22, -18, -15, -19, -13 };

		int count = 0;
		int count1 = 0;
		for (int i = 0; i < kraad1.length; i++) {

			if (kraad1[i] >= 0) {

				count++;
			}

			count1++;

		}

		int result = count1 - count;

		System.out.println("kraadid1 postiivsed numbrid mitukorda rohkem " + result);
		int count2 = 0;
		int count3 = 0;

		for (int i = 0; i < kraad2.length; i++) {

			if (kraad2[i] >= 0) {

				count2++;
			}

			count3++;

		}

		int result1 = count3 - count2;

		System.out.println("kraadid2 postiivsed numbrid mitukorda rohkem " + result1);

	}
}
