package eksamiUleasnedOOP;

/**
 * Siin failis kasutatakse objekti Kassa, aga Kassa klassi ei eksisteeri. Sinu �lesanne
 * on see luua.
 *
 * K�esolevat klassi ei tohi muuta - mitte �htegi t�hem�rki!
 *
 * Jep, sama �lesanne oli ka proovieksamis.
 */
public class Pood {

    public static void main(String[] args) {

        String kassapidaja = "Laine";
        Kassa kassa = new Kassa(kassapidaja);

        kassa.lisaToode("Piim");y
        kassa.lisaToode("Sai");
        kassa.lisaToode("Lillkapsas");
        kassa.lisaToode("Lamuu j��tis");
        kassa.lisaToode("Kanepik�psis");
        kassa.eemaldaToode("Piim");
        kassa.eemaldaToode("Sai");
        kassa.lisaToode("Leib");

        kassa.prindiOstutsekk();
        kassa.prindiKassapidajaNimi();
    }
}
