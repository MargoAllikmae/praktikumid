package eksamiUleasnedOOP;

/**
 * Loo Chat klass, et k�esolev programm saaks normaalselt toimida.
 *
 * On �ks reegel: Gitter klassi muuta ei tohi.
 */
public class Gitter {

    public static void main(String[] args) {

        String toaNimi = "Elutuba";
        Chat chat = new Chat(toaNimi);

        chat.sisestaSonum("Taavi", "Tere, on keegi siin?");
        chat.sisestaSonum("Eva", "Tsau, ilus poiss. Mis otsid?");
        chat.sisestaSonum("Taavi", "Kuule, ega sa ei tea kuidas seda OOP �lesannet lahendada?");
        chat.sisestaSonum("Taavi", "Pean siin nii kasutajanime kui ka s�numi kuhugi salvestama..");
        chat.sisestaSonum("Eva", "Oh sind totut, v�imalusi on ju mitu.");
        chat.sisestaSonum("Eva", "Aga pead ise v�lja m�tlema");
        chat.sisestaSonum("Eva", "sest �ppej�ud n�eb meie kirjutatut.");
        chat.sisestaSonum("Taavi", "Kurat ta j�lle nii raske eksami tegi..");

        chat.prindiKoikSonumidKoosKasutajanimega();

        chat.adminKustutabSonumi("Kurat ta j�lle nii raske eksami tegi..");
        chat.sisestaSonum("Krister", "Sul veab, et siin chatis v�lja viskamise funktsionaalsust ei ole :)");

        chat.prindiKoikSonumidKoosKasutajanimega();
        chat.prinditoaNimi();
    }
}