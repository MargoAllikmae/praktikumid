package eksamiUleasnedOOP;

/**
 * Loo see Pastaka objekt, mida kirjanik soovib siin kasutada.
 *
 * Reeglid:
 * 1. K�esolevat faili ei tohi muuta
 * 2. Kui pastakas kirjutab siis tuleb tekst konsooli - kuni tinti j�tkub.
 * 3. Iga t�ht kulutab �he �hiku.
 */
public class Kirjanik {
    public static void main(String[] args) {
        int tindiKogus = 60;

        Pastakas pastakas = new Pastakas(tindiKogus);

        pastakas.kirjuta("Elu oleks palju lihtsam, kui meil oleks selle l�htekood.");

        pastakas.prindiPaljuTintiAlles();

        pastakas.kirjuta("�iged progejad ei kommenteeri oma koodi. Kui seda oli raske kirjutada, siis peab olema seda ka raske lugeda!");

    }
}