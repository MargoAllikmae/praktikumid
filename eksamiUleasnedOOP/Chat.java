package eksamiUleasnedOOP;
import java.util.ArrayList;
import java.util.Arrays;


public class Chat {
	String nimi;

	ArrayList<String[]> messages = new ArrayList<String[]>();

	public Chat(String toaNimi) {
		nimi = toaNimi;
	}

	public void sisestaSonum(String kasutaja, String s) {
		String[] rida = new String[2];
		rida[0] = kasutaja;
		rida[1]=s;
		messages.add(rida);
		
	}

	public void prindiKoikSonumidKoosKasutajanimega() {
		for(int i =0; 1<messages.size();i++){
			System.out.println(Arrays.toString(messages.get(i)));
		}
		
	}

	public void adminKustutabSonumi(String kustutatav) {
		for(int i =0; 1<messages.size();i++){
				if (messages.get(i)[1].equals(kustutatav)) {
					messages.remove(i);
				}
		}
		
	}
	
	public void prinditoaNimi() {
		System.out.println(nimi);
		
	}

}
