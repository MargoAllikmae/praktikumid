package ekasmiUlesansdedJavaFX;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

/**
 * Joonista 500x500 ekraanile ring. Kui hiir ringile vastu l�heb, leiab ring omale
 * uue koha. Ei pea animeerima ega midagi f��nsit. Lihtsalt ilmub uues kohas ja k�ik.
 * Ehk hiirega saab ringi taga ajada. Kordan - ring leiab uue koha juba hiire puudutusest,
 * mitte klikist. Aga alustada v�id muidugi klikist.
 */

public class Tagaajamine extends Application{
	

	@Override
	public void start(Stage primaryStage) throws Exception {
		Pane pane = new Pane();
		@SuppressWarnings("unused")
		Scene scene = new Scene(pane, 500, 500);
		
		for (int i = 0; i < 10; i++) {
			Rectangle rect = new Rectangle(i*100, i*100, 100, 100);
			pane.getChildren().add(rect);
			rect.setOnMouseClicked(event ->{
				if (rect.getId() == ("klikitud")) {
					rect.setScaleX(0);
					rect.setScaleY(0);
				} else {
					rect.setScaleX(0.5);
					rect.setScaleY(0.5);
	
				}
				
				rect.setId("klikitud");
			});
			
		}
	
	}
	


}